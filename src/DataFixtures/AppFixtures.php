<?php

namespace App\DataFixtures;

use App\Entity\Album;
use App\Entity\Artist;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;


class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        
        
        $faker = Factory::create();

        $artist = new Artist();
        $artist->setName($faker->userName());
        $artist->setGenre('pop');
        $manager->persist($artist);

        for ($i=0; $i < 10; $i++) { 
            
            $album = new Album();
            $album->setName($faker->domainWord());
            $album->setCover($faker->imageUrl());
            $album->setReleaseDate($faker->dateTime());
            $album->setArtist($artist);
            $manager->persist($album);
        }

        $manager->flush();
    }
}
