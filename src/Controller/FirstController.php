<?php

namespace App\Controller;

use App\Form\PersonType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FirstController extends AbstractController
{
    #[Route('/first', name: 'app_first')]
    public function index(): Response
    {
        return $this->render('first/index.html.twig', [
            'maVariable' => 'FirstController',
        ]);
    }

    #[Route('/todo', name: 'app_todo')]
    public function todo(Request $request): Response
    {
        $todos = ['Task 1', 'Task 2', 'Task 3'];
        $newTodo = $request->get('newTodo');
        if($newTodo) {
            $todos[] = $newTodo;
        }

        return $this->render('first/todo.html.twig', [
            'maVariable' => $todos,
        ]);
    }

    #[Route('/person', name: 'app_person')]
    public function person(Request $request): Response
    {
        $form = $this->createForm(PersonType::class);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $person = $form->getData();
            dump($person);
            //Ici on ferait un save pour faire persister la person par ex
        }
        

        return $this->render('first/person.html.twig', [
            'form' => $form,
        ]);
    }
    
}
