<?php

namespace App\Controller;

use App\Entity\Album;
use App\Form\AlbumType;
use App\Repository\AlbumRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AlbumController extends AbstractController
{
    public function __construct(private AlbumRepository $repo) {}

    #[Route('/album', name: 'app_album')]
    public function index(): Response
    {
        return $this->render('album/index.html.twig', [
            'albums' => $this->repo->findAll()
        ]);
    }

    #[Route('/album/{id}', name: 'app_one_album')]
    public function one(Album $album): Response
    {
        return $this->render('album/one.html.twig', [
            'album' => $album
        ]);
    }
    

    #[Route('/add-album', name: 'app_add_album')]
    public function add(Request $request): Response
    {
        $album = new Album();
        $form = $this->createForm(AlbumType::class, $album);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $this->repo->save($album, true);

            return $this->redirectToRoute('app_album');
        }

        return $this->render('album/add.html.twig', [
            'form' => $form
        ]);
    }
}
